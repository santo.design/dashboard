import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { Router } from '@angular/router';
import * as screenfull from 'screenfull';

import { NgxPermissionsService } from 'ngx-permissions';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})

export class LayoutComponent implements OnDestroy {
  mobileQuery: MediaQueryList;
  events: string[] = [];
  opened: boolean;

  private _mobileQueryListener: () => void;

  constructor(
    private router: Router,
    private permissionsService: NgxPermissionsService,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  options = {
    minimum: 0.08,
    maximum: 1,
    ease: 'linear',
    speed: 200,
    trickleSpeed: 300,
    meteor: false,
    spinner: true,
    spinnerPosition: 'right',
    direction: 'leftToRightIncreased',
    color: 'white',
    thick: true
  };

  sideMenuProcess: any = [];
  sideMenuProcess2: any = [];
  sideMenuTree: any = [];
  selectedMenu = [];

  ngOnInit() {
    
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  toggleFullScreen(): void {
    if (screenfull.enabled) {
      screenfull.toggle();
    }
  }

  logout() {
    this.router.navigate(['login']);
    localStorage.clear();
  }
}
