import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { TreeModule } from 'angular-tree-component';
import { HttpModule } from '@angular/http';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

// primeng
import { FileUploadModule } from 'primeng/primeng';

// https://material.angular.io/
import * as mat from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPermissionsModule, NgxPermissionsService } from 'ngx-permissions';

@NgModule({
  imports: [
    NgxDatatableModule,
    CommonModule,
    mat.MatToolbarModule,
    mat.MatExpansionModule,
    mat.MatIconModule,
    mat.MatMenuModule,
    mat.MatButtonModule,
    mat.MatSidenavModule,
    mat.MatCheckboxModule,
    mat.MatListModule,
    mat.MatTooltipModule,
    mat.MatProgressBarModule,
    mat.MatCardModule,
    mat.MatTabsModule,
    mat.MatInputModule,
    mat.MatDatepickerModule,
    mat.MatNativeDateModule,
    mat.MatTooltipModule,
    mat.MatDialogModule,
    mat.MatSelectModule,
    mat.MatSlideToggleModule,
    mat.MatRadioModule,
    TreeModule,
    mat.MatChipsModule,
    FileUploadModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    mat.MatSnackBarModule,
    NgxPermissionsModule
  ],
  exports: [
    NgxDatatableModule,
    mat.MatToolbarModule,
    mat.MatProgressBarModule,
    mat.MatExpansionModule,
    mat.MatIconModule,
    mat.MatButtonModule,
    mat.MatSidenavModule,
    mat.MatCheckboxModule,
    mat.MatListModule,
    mat.MatTooltipModule,
    mat.MatCardModule,
    mat.MatMenuModule,
    mat.MatTabsModule,
    mat.MatInputModule,
    mat.MatDatepickerModule,
    mat.MatNativeDateModule,
    mat.MatTooltipModule,
    mat.MatDialogModule,
    mat.MatSelectModule,
    mat.MatSlideToggleModule,
    mat.MatRadioModule,
    TreeModule,
    mat.MatChipsModule,
    FileUploadModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    mat.MatSnackBarModule,
    NgxPermissionsModule
  ],
  declarations: [],
  providers: [
    NgxPermissionsService
  ],
})
export class ShareModule { }
