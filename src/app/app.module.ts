import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// routing
import { RouterModule } from '@angular/router';
import { routes } from './app-routing.module';

import 'hammerjs';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LayoutComponent } from './layout/layout.component';
import { MediaMatcher } from '@angular/cdk/layout';
import { ShareModule } from './share/share.module';
import { LoginComponent } from './login/login.component';

//ProgressModule
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpModule } from '@ngx-progressbar/http';
import { NgProgressRouterModule } from '@ngx-progressbar/router';
import { HttpClientModule } from '@angular/common/http';

//ngx permissions
import { NgxPermissionsModule } from 'ngx-permissions';


@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    ShareModule,
    RouterModule.forRoot(routes),
    NgxPermissionsModule.forRoot(),
    HttpClientModule,
    NgProgressModule.forRoot({
      spinnerPosition: 'left',
      color: '#ffffff',
      thick: true,
      spinner:false
    }),
    NgProgressHttpModule,
    NgProgressRouterModule
  ],
  exports: [ ],
  providers: [
    MediaMatcher
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
