import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private router: Router,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    
  }

  login() {

    let configSnackBar = new MatSnackBarConfig();
    configSnackBar.verticalPosition = "bottom";
    configSnackBar.duration = 5000;
    configSnackBar.panelClass = 'snackbar-class';

    let snackBarRef = this.snackBar.open('Login Successfully', 'close', configSnackBar);
    this.router.navigate(['/home']);

  }

}
