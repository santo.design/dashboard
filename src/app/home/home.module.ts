import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxChartsModule } from '@swimlane/ngx-charts';

//page
import { HomeComponent, HomeRoutingModule } from './home.component';

//module common
import { ShareModule } from '../share/share.module';

@NgModule({
  imports: [
    HomeRoutingModule,
    CommonModule,
    ShareModule,
    NgxChartsModule
  ],
  declarations: [ 
    HomeComponent
   ],
  entryComponents: [
      
  ],
  providers:[
    
  ]
})
export class HomeModule { }