import { Component, OnInit, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { single, multi, barTopData, areaTopData, lineTopData, stackBarData, dougnatHome } from './data';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  single: any[];
  multi: any[];
  barTopData: any[];
  areaTopData: any[];
  lineTopData: any[];
  pieTopData: any[];
  dougnatHome: any[];

  // bar top option
  viewBarTop: any[] = [200, 60];
  showXAxisBarTop = false;
  showYAxisBarTop = false;
  gradientBarTop = true;
  showLegendBarTop = false;
  showXAxisLabelBarTop = false;
  xAxisLabelBarTop = '';
  showYAxisLabelBarTop = false;
  yAxisLabelBarTop = '';
  colorSchemeBarTop = {
    domain: ['#E91E63']
  };

  // area top option
  viewAreaTop: any[] = [200, 60];
  showXAxisAreaTop = false;
  showYAxisAreaTop = false;
  gradientAreaTop = false;
  showLegendAreaTop = false;
  showXAxisLabelAreaTop = false;
  xAxisLabelAreaTop = '';
  showYAxisLabelAreaTop = false;
  yAxisLabelAreaTop = '';
  colorSchemeAreaTop = {
    domain: ['#E91E63']
  };

  // line top options
  viewLineTop: any[] = [200, 60];
  showXAxisLineTop = false;
  showYAxisLineTop = false;
  gradientLineTop = false;
  showLegendLineTop = false;
  showXAxisLabelLineTop = false;
  xAxisLabelLineTop = '';
  showYAxisLabelLineTop = false;
  yAxisLabelLineTop = '';
  colorSchemeLineTop = {
    domain: ['#E91E63']
  };

  // stackbar top option
  viewStackbarTop: any[] = [200, 60];
  showXAxisStackbarTop = false;
  showYAxisStackbarTop = true;
  gradientStackbarTop = false;
  showLegendStackbarTop = false;
  showXAxisLabelStackbarTop = false;
  xAxisLabelStackbarTop = '';
  showGridLinesStackbarTop = true;
  showYAxisLabeStackbarTop = false;
  yAxisLabelStackbarTop = '';
  colorSchemeStackbarTop = {
    domain: ['#E91E63', '#F06292']
  };

  // options donut toyota
  // view: any[] = [400, 200];
  showLegendDonut = false;
  colorSchemeDonut= {
    domain: ['#E91E63', '#D4E157', '#2196f3', '#1DE9B6']
  };
  showLabelsDonut = false;
  explodeSlicesDonut = false;
  doughnutDonut = false;


  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Population';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };
  
  // line, area
  autoScale = true;

  constructor() {
    Object.assign(this, { single, multi, barTopData, areaTopData, lineTopData, stackBarData, dougnatHome })
  }

  onSelect(event) {
    console.log(event);
  }

  ngOnInit() {

  }

}

//routing
const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    data: {
      title: 'Home'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class HomeRoutingModule { }
//routing end
