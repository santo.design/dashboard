export var single = [
    {
        "name": "Germany",
        "value": 8940000
    },
    {
        "name": "USA",
        "value": 5000000
    },
    {
        "name": "France",
        "value": 7200000
    }
];

export var barTopData = [
    {
        "name": "Avanza",
        "value": 100
    },
    {
        "name": "Calya",
        "value": 120
    },
    {
        "name": "Camry",
        "value": 32
    },
    {
        "name": "Rush",
        "value": 75
    },
    {
        "name": "Innova",
        "value": 80
    },
    {
        "name": "Agya",
        "value": 120
    }
];

export var areaTopData = [
    {
        "name": "Jawa",
        "series": [
            {
                "name": "Avanza",
                "value": 100
            },
            {
                "name": "Calya",
                "value": 120
            },
            {
                "name": "Camry",
                "value": 32
            },
            {
                "name": "Rush",
                "value": 75
            },
            {
                "name": "Innova",
                "value": 80
            },
            {
                "name": "Agya",
                "value": 120
            }
        ]
    },
    {
        "name": "Papua",
        "series": [
            {
                "name": "Avanza",
                "value": 6
            },
            {
                "name": "Calya",
                "value": 76
            },
            {
                "name": "Camry",
                "value": 33
            },
            {
                "name": "Rush",
                "value": 87
            },
            {
                "name": "Innova",
                "value": 22
            },
            {
                "name": "Agya",
                "value": 64
            }
        ]
    },
    {
        "name": "Maluku",
        "series": [
            {
                "name": "Avanza",
                "value": 6
            },
            {
                "name": "Calya",
                "value": 76
            },
            {
                "name": "Camry",
                "value": 33
            },
            {
                "name": "Rush",
                "value": 87
            },
            {
                "name": "Innova",
                "value": 22
            },
            {
                "name": "Agya",
                "value": 64
            }
        ]
    }
];

export var lineTopData = [
    {
        "name": "Area",
        "series": [
            {
                "name": "Jawa",
                "value": 100
            },
            {
                "name": "Papua",
                "value": 120
            },
            {
                "name": "Bali",
                "value": 32
            },
            {
                "name": "Lombok",
                "value": 75
            },
            {
                "name": "Maluku",
                "value": 80
            }
        ]
    }
];

export var stackBarData = [
    {
        "name": "Car",
        "series": [
            {
                "name": "November",
                "value": 73
            },
            {
                "name": "December",
                "value": 89
            }
        ]
    },

    {
        "name": "Motor Cycle",
        "series": [
            {
                "name": "November",
                "value": 78
            },
            {
                "name": "December",
                "value": 82
            }
        ]
    },

    {
        "name": "Boat",
        "series": [
            {
                "name": "November",
                "value": 50
            },
            {
                "name": "December",
                "value": 53
            }
        ]
    }
];

export var pieTopData = [
    {
        "name": "Car",
        "value": 100
    },
    {
        "name": "Motor Cycle",
        "value": 120
    },
    {
        "name": "Boar",
        "value": 30
    }
];

export var dougnatHome = [
    {
        "name": "100",
        "value": 100
    },
    {
        "name": "120",
        "value": 120
    },
    {
        "name": "30",
        "value": 30
    },
    {
        "name": "25",
        "value": 25
    }
];

export var multi = [
    {
        "name": "Germany",
        "series": [
            {
                "name": "2010",
                "value": 7300000
            },
            {
                "name": "2011",
                "value": 8940000
            }
        ]
    },

    {
        "name": "USA",
        "series": [
            {
                "name": "2010",
                "value": 7870000
            },
            {
                "name": "2011",
                "value": 8270000
            }
        ]
    },

    {
        "name": "France",
        "series": [
            {
                "name": "2010",
                "value": 5000002
            },
            {
                "name": "2011",
                "value": 5800000
            }
        ]
    }
];

export var lineHomeData = [
    {
        "name": "Car",
        "series": [
            {
                "name": "Week 1",
                "value": 330
            },
            {
                "name": "Week 2",
                "value": 390
            },
            {
                "name": "Week 3",
                "value": 370
            },
            {
                "name": "Week 4",
                "value": 385
            }
        ]
    },
    {
        "name": "Motor Cycle",
        "series": [
            {
                "name": "Week 1",
                "value": 370
            },
            {
                "name": "Week 2",
                "value": 360
            },
            {
                "name": "Week 3",
                "value": 389
            },
            {
                "name": "Week 4",
                "value": 324
            }
        ]
    },
    {
        "name": "Boat",
        "series": [
            {
                "name": "Week 1",
                "value": 340
            },
            {
                "name": "Week 2",
                "value": 340
            },
            {
                "name": "Week 3",
                "value": 349
            },
            {
                "name": "Week 4",
                "value": 344
            }
        ]
    }
];

export var mainBarHome = [
    {
        "name": "HA MDO",
        "series": [
            {
                "name": "HA",
                "value": 64
            },
            {
                "name": "TS",
                "value": 8
            },
            {
                "name": "DATA",
                "value": 54
            }
        ]
    },
    {
        "name": "HA GTO",
        "series": [
            {
                "name": "HA",
                "value": 54
            },
            {
                "name": "TS",
                "value": 7
            },
            {
                "name": "DATA",
                "value": 54
            }
        ]
    },
    {
        "name": "HA PAL",
        "series": [
            {
                "name": "HA",
                "value": 54
            },
            {
                "name": "TS",
                "value": 5
            },
            {
                "name": "DATA",
                "value": 45
            }
        ]
    },
    {
        "name": "HA KTM",
        "series": [
            {
                "name": "HA",
                "value": 22
            },
            {
                "name": "TS",
                "value": 3
            },
            {
                "name": "DATA",
                "value": 65
            }
        ]
    },
    {
        "name": "HA ABN",
        "series": [
            {
                "name": "HA",
                "value": 65
            },
            {
                "name": "TS",
                "value": 10
            },
            {
                "name": "DATA",
                "value": 65
            }
        ]
    },
    {
        "name": "HA JYP",
        "series": [
            {
                "name": "HA",
                "value": 65
            },
            {
                "name": "TS",
                "value": 30
            },
            {
                "name": "DATA",
                "value": 66
            }
        ]
    },
    {
        "name": "HA LMK",
        "series": [
            {
                "name": "HA",
                "value": 33
            },
            {
                "name": "TS",
                "value": 10
            },
            {
                "name": "DATA",
                "value": 34
            }
        ]
    },
    {
        "name": "HA MDR",
        "series": [
            {
                "name": "HA",
                "value": 34
            },
            {
                "name": "TS",
                "value": 6
            },
            {
                "name": "DATA",
                "value": 65
            }
        ]
    },
    {
        "name": "HA MRK",
        "series": [
            {
                "name": "HA",
                "value": 64
            },
            {
                "name": "TS",
                "value": 0
            },
            {
                "name": "DATA",
                "value": 23
            }
        ]
    },
    {
        "name": "HA TNT",
        "series": [
            {
                "name": "HA",
                "value": 56
            },
            {
                "name": "TS",
                "value": 8
            },
            {
                "name": "DATA",
                "value": 65
            }
        ]
    },
    {
        "name": "HA BIK",
        "series": [
            {
                "name": "HA",
                "value": 46
            },
            {
                "name": "TS",
                "value": 4
            },
            {
                "name": "DATA",
                "value": 22
            }
        ]
    },
    {
        "name": "HA TMK",
        "series": [
            {
                "name": "HA",
                "value": 56
            },
            {
                "name": "TS",
                "value": 0
            },
            {
                "name": "DAT",
                "value": 45
            }
        ]
    },
    {
        "name": "HA SCR",
        "series": [
            {
                "name": "HA",
                "value": 56
            },
            {
                "name": "TS",
                "value": 4
            },
            {
                "name": "DATA",
                "value": 65
            }
        ]
    },
    {
        "name": "HA MRS",
        "series": [
            {
                "name": "HA",
                "value": 46
            },
            {
                "name": "TS",
                "value": 4
            },
            {
                "name": "DATA",
                "value": 22
            }
        ]
    },
    {
        "name": "HA JKT",
        "series": [
            {
                "name": "HA",
                "value": 56
            },
            {
                "name": "TS",
                "value": 4
            },
            {
                "name": "DATA",
                "value": 45
            }
        ]
    },
    {
        "name": "HA TBL",
        "series": [
            {
                "name": "HA",
                "value": 56
            },
            {
                "name": "TS",
                "value": 2
            },
            {
                "name": "DATA",
                "value": 65
            }
        ]
    }
];


