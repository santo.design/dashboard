import { Component, OnInit, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { single, multi, barTopData, lineHomeData, areaTopData, lineTopData, stackBarData, dougnatHome, mainBarHome } from './data';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  searchData = false;
  single: any[];
  multi: any[];
  barTopData: any[];
  areaTopData: any[];
  lineTopData: any[];
  pieTopData: any[];
  dougnatHome: any[];
  mainBarHome: any[];
  lineHomeData: any[];

  toyotaShow = true;
  yamahaShow = false;
  yamahaBoatShow = false;

  // bar top option
  viewBarTop: any[] = [200, 60];
  showXAxisBarTop = false;
  showYAxisBarTop = false;
  gradientBarTop = true;
  showLegendBarTop = false;
  showXAxisLabelBarTop = false;
  xAxisLabelBarTop = '';
  showYAxisLabelBarTop = false;
  yAxisLabelBarTop = '';
  colorSchemeBarTop = {
    domain: ['#E91E63']
  };

  // area top option
  viewAreaTop: any[] = [200, 60];
  showXAxisAreaTop = false;
  showYAxisAreaTop = false;
  gradientAreaTop = false;
  showLegendAreaTop = false;
  showXAxisLabelAreaTop = false;
  xAxisLabelAreaTop = '';
  showYAxisLabelAreaTop = false;
  yAxisLabelAreaTop = '';
  colorSchemeAreaTop = {
    domain: ['#E91E63']
  };

  // line top options
  viewLineTop: any[] = [200, 60];
  showXAxisLineTop = false;
  showYAxisLineTop = false;
  gradientLineTop = false;
  showLegendLineTop = false;
  showXAxisLabelLineTop = false;
  xAxisLabelLineTop = '';
  showYAxisLabelLineTop = false;
  yAxisLabelLineTop = '';
  colorSchemeLineTop = {
    domain: ['#E91E63']
  };

  // stackbar top option
  viewStackbarTop: any[] = [200, 60];
  showXAxisStackbarTop = false;
  showYAxisStackbarTop = true;
  gradientStackbarTop = false;
  showLegendStackbarTop = false;
  showXAxisLabelStackbarTop = false;
  xAxisLabelStackbarTop = '';
  showGridLinesStackbarTop = true;
  showYAxisLabeStackbarTop = false;
  yAxisLabelStackbarTop = '';
  colorSchemeStackbarTop = {
    domain: ['#E91E63', '#F06292']
  };

  // options donut toyota
  // view: any[] = [400, 200];
  showLegendDonut = false;
  tooltipDisabledDonut = true;
  colorSchemeDonut= {
    domain: ['#E91E63', '#D4E157', '#2196f3', '#1DE9B6']
  };
  showLabelsDonut = true;
  explodeSlicesDonut = false;
  doughnutDonut = true;


  // view: any[] = [1030, 277];

  // options bar
  showXAxisBar = true;
  showYAxisBar = true;
  gradientBar = false;
  groupPaddingBar = 2;
  barPaddingBar = 4;
  yScaleMaxBar = 2;
  showDataLabelBar = true;
  showLegendBar = false;
  showXAxisLabelBar = false;
  xAxisLabelBar = '';
  tooltipDisabledBar = true;
  showYAxisLabelBar = true;
  yAxisLabelBar = 'Unit-Sales (Unit)';
  colorSchemeBar = {
    domain: ['#2196f3', '#E91E63', '#1DE9B6', '#ffffff']
  };

  // view: any[] = [700, 400];
  // options
  showXAxis = true;
  showYAxis = false;
  gradient = false;
  showLegend = false;
  showXAxisLabel = false;
  xAxisLabel = '';
  showYAxisLabel = false;
  yAxisLabel = '';

  colorScheme = {
    domain: ['#E91E63', '#1DE9B6', '#2196f3', '#ffffff']
  };AD1457
  
  // line, area
  autoScale = true;

  constructor() {
    Object.assign(this, { single, multi, lineHomeData, barTopData, areaTopData, lineTopData, stackBarData, dougnatHome, mainBarHome })
  }

  onSelect(event) {
    console.log(event);
  }

  ngOnInit() {

  }

  changeSummary (data) {
    if (data==='toyota') {
      this.toyotaShow = true;
      this.yamahaShow = false;
      this.yamahaBoatShow = false;
    }
    else if (data==='yamaha') {
      this.toyotaShow = false;
      this.yamahaShow = true;
      this.yamahaBoatShow = false;
    }
    else {
      this.toyotaShow = false;
      this.yamahaShow = false;
      this.yamahaBoatShow = true;
    }
  }

  searchBarData () {
    if (this.searchData===false) {
      this.searchData = true;
    }
    else {
      this.searchData = false;
    }
  }

}

//routing
const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    data: {
      title: 'Home'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class HomeRoutingModule { }
//routing end
